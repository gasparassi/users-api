<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest as FormRequest;
use App\Models\Person;
use Illuminate\Validation\Rule;

class PersonUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Rule::unique((new Person())->getTable())->ignore($this->route()->person->id ?? null),
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'email', 'max:50',],
        ];
    }
}
