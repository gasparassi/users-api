<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Contracts\{
    CategoryServiceInterface,
    PersonServiceInterface,
};
use App\Services\{
    CategoryService,
    PersonService,
};

class CustomServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CategoryServiceInterface::class,
            CategoryService::class,
        );

        $this->app->bind(
            PersonServiceInterface::class,
            PersonService::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
