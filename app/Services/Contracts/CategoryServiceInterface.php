<?php

namespace App\Services\Contracts;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface CategoryServiceInterface
{
    public function getAll(): LengthAwarePaginator;
    
    public function getAllForSelect(): Collection;

    public function getOne(int $id): ?Category;

    public function getOneAndUpdate(array $attributes, int $id): Category;

    public function create(array $attributes): ?Category;

    public function getAndDelete(int $id): bool;
}
