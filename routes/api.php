<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PersonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get(
    '/',
    function () {
        return response()
            ->json(
                [
                    'message' => 'Usuários API',
                    'status' => 'Connected',
                ],
                200
            );
    }
);

Route::prefix('v1')->group(function () {

    Route::get('categories/select', [CategoryController::class, 'indexForSelect']);

    Route::resource('categories', CategoryController::class)->except('create', 'edit');
    Route::resource('persons', PersonController::class)->except('create', 'edit');
});
